import "./App.css";
import React, { useEffect, useState, useRef } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import {
  Navbar,
  Container,
  Button,
  Nav,
  Row,
  Col,
  Form,
  Card,
} from "react-bootstrap";
import CarsLogo from "../public/img/img_car.png";
import Facebook from "../public/img/icon_facebook.png";
import Instagram from "../public/img/icon_instagram.png";
import Twitter from "../public/img/icon_twitter.png";
import Mail from "../public/img/icon_mail.png";
import Twitch from "../public/img/icon_twitch.png";
import { BsCalendar3, BsPeopleFill, BsGearFill } from "react-icons/bs";
import { useDispatch } from "react-redux";
import { addUser } from "../slices/userSlice";

function Filter() {
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [user, setUser] = useState({});
  const [cars, setCars] = useState([]);
  const navigate = useNavigate();
  const isWithDriver = useRef();
  const availableAtDate = useRef();
  const availableAtTime = useRef();
  const capacity = useRef();
  const dispatch = useDispatch();

  const styleButton = {
    borderRadius: "0px",
  };

  const styleButton2 = {
    backgroundColor: "#5cb85f",
    borderRadius: "0px",
  };

  const navStyle = {
    padding: "8px 10px",
  };

  const styleP = {
    fontSize: "14px",
  };

  const showCars = async () => {
    //fetching
    const response = await axios.get("http://localhost:1010/cars/show");
    //get response data
    const data = await response.data.data.Loaded_Car;
    console.log(data);

    //assign response data to state "posts"
    setCars(data);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Check status user login
        // 1. Get token from localStorage
        const token = localStorage.getItem("token");

        // 2. Check token validity from API
        const currentUserRequest = await axios.get(
          "http://localhost:1010/auth/me",
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );

        const currentUserResponse = currentUserRequest.data;

        if (currentUserResponse.status) {
          dispatch(
            addUser({
              user: currentUserResponse.data.user,
              token: token,
            })
          );
          setUser(currentUserResponse.data.user);
        }
      } catch (err) {
        setIsLoggedIn(false);
      }
    };

    fetchData();
    showCars();
  }, []);

  const onFilter = async (e) => {
    e.preventDefault();

    try {
      // const response = await axios.get(
      //   "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json"
      // );
      const dateTime = new Date(
        `${availableAtDate.current.value} ${availableAtTime.current.value}`
      );
      const response = await axios.get(
        `http://localhost:1010/cars/filter?isWithDriver=${
          isWithDriver.current.value
        }&availableAt=${dateTime.toISOString()}&capacity=${
          capacity.current.value
        }`
      );

      //get response data
      const data = await response.data.data.Cars;
      console.log(data);
      // console.log(availableAt);
      // const data = await response.data;

      //assign response data to state "cars"
      // const inputDateValue = availableAt.current.value;
      // const inputTimeValue = availableAtTime.current.value;
      // const inputPassangersValue = capacity.current.value;

      // console.log("Date", inputDateValue);
      // console.log("Time", inputTimeValue);
      // console.log("Penumpang", inputPassangersValue);

      // console.log("Date Time:", dateTime);

      // const epochTime = dateTime.getTime();
      // console.log("epochTime:", epochTime);
      // const filterCars = await data.filter((car) => {
      //   if (
      //     car.capacity <= inputPassangersValue &&
      //     car.availableAt < epochTime
      //   ) {
      //     return car;
      //   }
      // });

      // console.log(filterCars);
      setCars(data);
    } catch (err) {
      console.log(err);
      const response = err.response.data;
      console.log(response);
    }
  };

  const logout = () => {
    localStorage.removeItem("token");

    setIsLoggedIn(false);
    setUser({});

    navigate("/");
  };

  return isLoggedIn ? (
    <>
      {/* navbar */}
      <Navbar fixed="top" className="navbar">
        <Container>
          <Link to="/" className="box me-3"></Link>
          <Button
            style={styleButton}
            size="sm"
            disabled
            variant="outline-secondary"
            className="text-black fw-bold "
          >
            {user.name}
          </Button>

          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end text-decoration-none ">
            <Nav.Link href="#" className="text-black" style={navStyle}>
              Our Service
            </Nav.Link>
            <Nav.Link href="#" className="text-black" style={navStyle}>
              Why Us
            </Nav.Link>
            <Nav.Link href="#" className="text-black" style={navStyle}>
              Testimonial
            </Nav.Link>
            <Nav.Link href="#" className="text-black" style={navStyle}>
              FAQ
            </Nav.Link>

            <Nav.Link className="text-black" style={navStyle}>
              <Button
                variant="outline-danger"
                style={styleButton}
                onClick={(e) => logout(e)}
              >
                Logout
              </Button>
            </Nav.Link>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/* navbar */}

      {/* Hero */}
      <section className="hero">
        <Container>
          <Row>
            <Col md={6} className="hero-text">
              <p className="title">
                Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
              </p>
              <p className="content">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
            </Col>
            <Col md={6} className="picture">
              <img src={CarsLogo} alt="llog of cars" />
            </Col>
          </Row>
        </Container>
      </section>
      {/* Hero */}

      {/* form */}
      <section className="form">
        <Container style={{ textAlign: "-webkit-center" }}>
          <Form onSubmit={onFilter} className="d-flex form-content gap-3 px-3">
            {/* Driver */}
            <Form.Group className="mb-3 w-25">
              <Form.Label style={styleP}>Tipe Driver</Form.Label>
              <Form.Select style={styleP} ref={isWithDriver}>
                <option hidden>Pilih Tipe Driver</option>
                <option value="true">Dengan Sopir</option>
                <option value="false">Tanpa Sopir (Lepas Kunci)</option>
              </Form.Select>
            </Form.Group>
            {/* Driver */}

            {/* Date */}
            <Form.Group className="mb-3 w-25">
              <Form.Label style={styleP}>Tanggal</Form.Label>
              <input
                placeholder="Pilih Tanggal"
                type="date"
                className="form-control"
                id="inputDate"
                ref={availableAtDate}
              />
            </Form.Group>
            {/* Date */}

            {/* TIme */}
            <Form.Group className="mb-3 w-25">
              <Form.Label style={styleP}>Waktu Jemput/ Ambil</Form.Label>
              <Form.Select style={styleP} ref={availableAtTime}>
                <option hidden>Pilih Waktu</option>
                <option value="08:00">08:00 WIB</option>
                <option value="09:00">09:00 WIB</option>
                <option value="10:00">10:00 WIB</option>
                <option value="11:00">11:00 WIB</option>
                <option value="12:00">12:00 WIB</option>
                <option value="13:00">13:00 WIB</option>
                <option value="14:00">14:00 WIB</option>
                <option value="15:00">15:00 WIB</option>
                <option value="16:00">16:00 WIB</option>
                <option value="17:00">17:00 WIB</option>
                <option value="18:00">18:00 WIB</option>
                <option value="19:00">19:00 WIB</option>
                <option value="20:00">20:00 WIB</option>
                <option value="21:00">21:00 WIB</option>
                <option value="22:00">22:00 WIB</option>
                <option value="23:00">23:00 WIB</option>
                <option value="24:00">24:00 WIB</option>
              </Form.Select>
            </Form.Group>
            {/* Time */}

            {/* Capacity */}
            <Form.Group className="mb-3 w-25">
              <Form.Label style={styleP}>
                Jumlah Penumpang (Optional)
              </Form.Label>
              <Form.Select style={styleP} ref={capacity}>
                <option hidden>Jumlah Penumpang</option>
                <option value="1">1 orang</option>
                <option value="2">2 orang</option>
                <option value="3">3 orang</option>
                <option value="4">4 orang</option>
                <option value="5">5 orang</option>
                <option value="6">6 orang</option>
              </Form.Select>
            </Form.Group>
            {/* Capacity */}

            <Button
              variant="success"
              className="mt-3"
              type="submit"
              style={styleButton2}
            >
              Submit
            </Button>
            {/* <Button variant="success" style={styleButton2}>
              Clear
            </Button> */}
          </Form>
        </Container>
      </section>
      {/* form */}

      {/* card */}
      <Container>
        <Row>
          {cars.map((car) => (
            <Col md={4} key={car.id}>
              <Card
                style={{ marginTop: "2rem" }}
                key={car.id}
                className="card-content__cars"
              >
                <img src={car.image} className="card-image__cars" alt="" />

                <div className="card-body">
                  <p>
                    {car.model} / {car.manufacture}
                  </p>
                  <h5 className="card-title bold">
                    Rp {car.rentPerDay} / hari
                  </h5>
                  <p
                    className="card-text"
                    style={{ height: "130px", textAlign: "justify" }}
                  >
                    {car.description}
                  </p>
                  <div className="">
                    <BsPeopleFill className="me-2" />
                    {car.capacity} Orang
                  </div>
                  <div className="pt-2">
                    <BsGearFill className="me-2" />
                    {car.transmission}
                  </div>
                  <div className="pt-2">
                    <BsCalendar3 className="me-2" />
                    Tahun {car.year}
                  </div>
                  <Card.Link href="#">
                    <Button variant="secondary" className=" w-100 mt-3">
                      Pilih Mobil
                    </Button>
                  </Card.Link>
                </div>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
      {/* card */}

      {/* footer */}
      <section className="footer">
        <Container>
          <footer>
            <Row>
              <Col md={3} style={styleP}>
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
              </Col>
              <Col md={2} style={styleP} className="footer-link">
                <p>
                  <a href="#our-service">Our Service</a>
                </p>
                <p>
                  <a href="#why-us">Why Us</a>
                </p>
                <p>
                  <a href="#testimonial">Testimonial</a>
                </p>
                <p>
                  <a href="#faq">FAQ</a>
                </p>
              </Col>
              <Col md={3}>
                <p style={styleP}>Connect with us</p>
                <p className="icons" style={styleP}>
                  <img src={Facebook} alt="gambar facebook" />
                  <img src={Instagram} alt="gambar insta" />
                  <img src={Twitter} alt="gambar twit" />
                  <img src={Mail} alt="gambar mail" />
                  <img src={Twitch} alt="gambar twitch" />
                </p>
              </Col>
              <Col md={4}>
                <p style={styleP}>Copyright Binar 2022</p>
                <Col className="box"></Col>
              </Col>
            </Row>
          </footer>
        </Container>
      </section>
      {/* footer */}
    </>
  ) : (
    <Navigate to="/login" />
  );
}

export default Filter;
